/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80028
 Source Host           : localhost:3306
 Source Schema         : dynamic-datasource

 Target Server Type    : MySQL
 Target Server Version : 80028
 File Encoding         : 65001

 Date: 11/05/2022 18:05:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for datasource_driver_info
-- ----------------------------
DROP TABLE IF EXISTS `datasource_driver_info`;
CREATE TABLE `datasource_driver_info`  (
  `driver_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '驱动ID',
  `driver_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '驱动名称',
  PRIMARY KEY (`driver_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of datasource_driver_info
-- ----------------------------
INSERT INTO `datasource_driver_info` VALUES ('1', 'com.mysql.cj.jdbc.Driver');
INSERT INTO `datasource_driver_info` VALUES ('2', 'com.mysql.jdbc.Driver');
INSERT INTO `datasource_driver_info` VALUES ('3', 'oracle.jdbc.driver.OracleDriver');
INSERT INTO `datasource_driver_info` VALUES ('4', 'dm.jdbc.driver.DmDriver');

-- ----------------------------
-- Table structure for datasource_info
-- ----------------------------
DROP TABLE IF EXISTS `datasource_info`;
CREATE TABLE `datasource_info`  (
  `datasource_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数据源ID',
  `driver_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '驱动ID',
  `datasource_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数据源URL',
  `datasource_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数据源类型',
  `datasource_username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `datasource_password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户密码',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  `status` int NOT NULL COMMENT '0连接成功1连接失败',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`datasource_id`) USING BTREE,
  INDEX `driverID`(`driver_id`) USING BTREE,
  CONSTRAINT `driverID` FOREIGN KEY (`driver_id`) REFERENCES `datasource_driver_info` (`driver_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of datasource_info
-- ----------------------------
INSERT INTO `datasource_info` VALUES ('EfTlg-1DMZcpCGNAdIw5I', '1', 'jdbc:mysql://127.0.0.1:3306/dynamic-datasource?characterEncoding=utf8&useSSL=false&autoReconnect=true&allowPublicKeyRetrieval=true&serverTimezone=GMT%2B8', 'mysql', 'root', '123456', '2022-05-10 20:24:30', '2022-05-10 20:24:30', 0, 'mysql8.0.19');
INSERT INTO `datasource_info` VALUES ('X_j3TZCCzAlh_sMhR3-eK', '1', 'jdbc:mysql://127.0.0.1:3306/dynamic?characterEncoding=utf8&useSSL=false&autoReconnect=true&allowPublicKeyRetrieval=true&serverTimezone=GMT%2B8', 'mysql', 'root', '123456', '2022-05-10 20:30:00', '2022-05-10 20:30:00', 1, 'mysql8.0.19');

-- ----------------------------
-- Table structure for test
-- ----------------------------
DROP TABLE IF EXISTS `test`;
CREATE TABLE `test`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `age` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of test
-- ----------------------------
INSERT INTO `test` VALUES (1, 'ceshi1', 22);

SET FOREIGN_KEY_CHECKS = 1;
