package com.suyun.demo.constant;

/**
 * @author ytz
 * @apiNote 数据源常量配置
 */
public class DynamicDataSourceConstant {
    /**
     * oracle 驱动名称
     */
    public static final String ORACLE_DRIVER_CLASS_NAME = "oracle.jdbc.driver.OracleDriver";

    /**
     * mysql 低版本驱动名称(6.0以下)
     */
    public static final String MYSQL_LOW_DRIVER_CLASS_NAME = "com.mysql.jdbc.Driver";

    /**
     * mysql 高版本驱动名称(6.0以上)
     */
    public static final String MYSQL_HIGH_DRIVER_CLASS_NAME = "com.mysql.cj.jdbc.Driver";
}
