package com.suyun.demo.service.impl;

import com.baomidou.dynamic.datasource.DynamicRoutingDataSource;
import com.baomidou.dynamic.datasource.creator.DefaultDataSourceCreator;
import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DataSourceProperty;
import com.baomidou.dynamic.datasource.toolkit.DynamicDataSourceContextHolder;
import com.suyun.demo.common.DsConnVerify;
import com.suyun.demo.constant.DynamicDataSourceConstant;
import com.suyun.demo.dto.DynamicDataSourceDTO;
import com.suyun.demo.mapper.TestMapper;
import com.suyun.demo.service.DynamicDataSourceService;
import com.suyun.demo.utils.ResponseResult;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author ytz
 */
@Service
public class DynamicDataSourceServiceImpl implements DynamicDataSourceService {

    @Autowired
    private DataSource dataSource;
    @Autowired
    private DefaultDataSourceCreator dataSourceCreator;

    @Autowired
    private TestMapper testMapper;

    @Override
    public ResponseResult addDataSource(DynamicDataSourceDTO sourceDTO) {
        // 根据数据库类型设置驱动名称
        switch (sourceDTO.getType().toLowerCase()) {
            case "mysql":
                sourceDTO.setDriverClassName(DynamicDataSourceConstant.MYSQL_HIGH_DRIVER_CLASS_NAME);
                break;
            case "oracle":
                sourceDTO.setDriverClassName(DynamicDataSourceConstant.ORACLE_DRIVER_CLASS_NAME);
                break;
                default:
                    return null;
        }
        boolean status = DsConnVerify.verifyUrlConnStatus(sourceDTO.getUrl(), sourceDTO.getDriverClassName(), sourceDTO.getUsername(), sourceDTO.getUsername());
        if (!status) {
            return ResponseResult.fail("请检查数据源URL、用户名以及用户密码是否正确!");
        }
        DataSourceProperty dataSourceProperty = new DataSourceProperty();
        BeanUtils.copyProperties(sourceDTO, dataSourceProperty);
        DynamicRoutingDataSource ds = (DynamicRoutingDataSource) dataSource;
        DataSource dataSource = dataSourceCreator.createDataSource(dataSourceProperty);
        try {
            Connection connection = dataSource.getConnection();
            String schema = connection.getSchema();
            System.err.println(schema);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        // 添加数据源
        ds.addDataSource(sourceDTO.getPoolName(), dataSource);
        return ResponseResult.success(ds.getDataSources().keySet());
    }

    @Override
    public ResponseResult getAllDataSource() {
        DynamicRoutingDataSource ds = (DynamicRoutingDataSource) dataSource;
        return ResponseResult.success(ds.getDataSources().keySet());
    }

    @Override
    public ResponseResult removeByDataSourceByName(String dataSourceName) {
        DynamicRoutingDataSource ds = (DynamicRoutingDataSource) dataSource;
        ds.removeDataSource(dataSourceName);
        return ResponseResult.success();
    }

    @Override
    public ResponseResult selectAllByDataSourceByName(String dataSourceName) {
        // 手动切换到该数据源
        DynamicDataSourceContextHolder.push(dataSourceName);
        return ResponseResult.success(testMapper.selectAll());
    }
}
