package com.suyun.demo.service;

import com.suyun.demo.dto.DynamicDataSourceDTO;
import com.suyun.demo.utils.ResponseResult;

/**
 * @author ytz
 */
public interface DynamicDataSourceService {
    ResponseResult addDataSource(DynamicDataSourceDTO sourceDTO);

    ResponseResult getAllDataSource();

    ResponseResult removeByDataSourceByName(String dataSourceName);

    ResponseResult selectAllByDataSourceByName(String dataSourceName);
}
