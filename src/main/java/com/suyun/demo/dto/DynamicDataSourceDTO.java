package com.suyun.demo.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author ytz
 * @apiNote 动态添加数据源DTO
 */
@Data
public class DynamicDataSourceDTO{

    @NotBlank
    @ApiModelProperty(value = "连接池名称")
    private String poolName;

    @NotBlank
    @ApiModelProperty(value = "数据源类型")
    private String type;

    @ApiModelProperty(value = "驱动名称",hidden = true)
    private String driverClassName;


    @NotBlank
    @ApiModelProperty(value = "url连接串")
    private String url;


    @NotBlank
    @ApiModelProperty(value = "用户名")
    private String username;


    @NotBlank
    @ApiModelProperty(value = "密码")
    private String password;
}
