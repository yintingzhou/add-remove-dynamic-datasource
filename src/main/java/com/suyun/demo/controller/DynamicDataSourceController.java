package com.suyun.demo.controller;

import com.suyun.demo.dto.DynamicDataSourceDTO;
import com.suyun.demo.service.DynamicDataSourceService;
import com.suyun.demo.utils.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author ytz
 * @apiNote 动态添加数据源控制层
 */
@RestController
@RequestMapping("/dynamic")
@Api(tags = "动态添加数据源API")
public class DynamicDataSourceController {
    @Autowired
    private DynamicDataSourceService dynamicDataSourceService;

    /**
     * 获取当前所有数据源
     */
    @ApiOperation(value = "获取当前所有数据源")
    @GetMapping("/getAllDataSource")
    public ResponseResult getAllDataSource() {
        return dynamicDataSourceService.getAllDataSource();
    }

    @ApiOperation(value = "添加数据源")
    @PostMapping("/addDataSource")
    public ResponseResult addDataSource(@Validated @RequestBody DynamicDataSourceDTO sourceDTO) {
        return dynamicDataSourceService.addDataSource(sourceDTO);
    }

    /**
     * 删除数据源
     */
    @ApiOperation(value = "根据数据源名称删除数据源")
    @GetMapping("/removeByDataSourceByName")
    public ResponseResult removeByDataSourceByName(@RequestParam String dataSourceName) {
        return dynamicDataSourceService.removeByDataSourceByName(dataSourceName);
    }

    @ApiOperation(value = "根据数据源名称查询测试数据")
    @GetMapping("/selectAllByDataSourceByName")
    public ResponseResult selectAllByDataSourceByName(@RequestParam(defaultValue = "master") String dataSourceName) {
        return dynamicDataSourceService.selectAllByDataSourceByName(dataSourceName);
    }
}
