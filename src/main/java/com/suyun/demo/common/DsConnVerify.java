package com.suyun.demo.common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author Administrator
 * @date 2022/04/24
 * @apiNote 检验数据库连接
 */
public class DsConnVerify {

    /**
     * 校验数据源连接是否成功
     * @param url 数据源URL
     * @param username 用户名
     * @param password 密码
     * @return jdbc:mysql://127.0.0.1:3306/dynamic-datasource?characterEncoding=utf8&useSSL=false&autoReconnect=true&allowPublicKeyRetrieval=true&serverTimezone=GMT%2B8
     */
    public static boolean verifyUrlConnStatus(String url,String driverClassName, String username, String password) {

        boolean flag;
        Connection connection = null;
        // 加载驱动类
        try {
            Class.forName(driverClassName);
            connection = DriverManager.getConnection(url, username, password);
            flag = true;
        } catch (Exception e) {
            e.printStackTrace();
            flag = false;
        }finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return flag;
    }
}
